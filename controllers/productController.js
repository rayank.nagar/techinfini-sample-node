
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const Pro = require("../model/product");
const ObjectId = require('mongodb').ObjectId; 


const getproducts = async (req, res) => {
    var token = req.headers['x-access-token'];
    if (!token){ 
                return res.status(401).json({ auth: false, message: 'No token provided.' });
               }
       jwt.verify(token, process.env.TOKEN_KEY, function(err, decoded) {
            if (err) {
                   return res.status(500).json({
                    status:false,
                    message:'Failed to authenticate token',
                    error:err
                   })
            }
            else{
                    Pro.find()
                    .then(result => {
                        res.status(200).json({
                            status:true,
                            message:'sucess',
                            ProductData:result
                        });
                    })
                    .catch(err=> {
                        res.status(500).json({
                            status:false,
                            message:'unsucess',
                            error:err
                        })
                    });
                }
    });
    
}



const productdetail = async (req, res) => {
    var token = req.headers['x-access-token'];
    if (!token){ 
                return res.status(401).json({ auth: false, message: 'No token provided.' });
               }
            jwt.verify(token, process.env.TOKEN_KEY, function(err, decoded) {
            if (err) {
                   return res.status(500).json({
                    status:false,
                    message:'Failed to authenticate token',
                    error:err
                   })
            }
            else{  
                    productid = req.params.productid;
                    var pro_id = new ObjectId(productid);
                    Pro.find().find({ _id: pro_id })
                    .then(result => {
                    res.status(200).json({
                        status:true,
                        message:'sucess',
                        ProductDetail:result
                    });
                    })
                    .catch(err=> {
                        res.status(500).json({
                            status:false,
                            message:'unsucess',
                            error:err
                        })
                    });
                }
            });
}

  module.exports = { getproducts, productdetail };
